// Global killswitch: only run if we are in a supported browser.
if (Drupal.jsEnabled) {
	$(document).ready( function() {
		$(".html2pdf-progress").each( function() {
			var id = $(this).attr("id").replace("html2pdf-progress-", "");
			var path = "/node/" + id + "/html2pdf_ajax";
			jQuery.get(path, null, function(data, type) {
				var res = Drupal.parseJson(data);
				if (res.nid > 0) {
					$("#html2pdf-progress-" + res.nid).html(res.message);
					if (res.status == "error") {
						$("#html2pdf-progress-" + res.nid).css('color', 'red');
					}
				} else {
					if (id > 0) {
						$("#html2pdf-progress-" + id).html("An error occurred during PDF file generation for node " + id + ". Please, contact an administrator.").css('color', 'red');
					}
					else {
						alert("An error occurred during PDF file generation. Please, contact an administrator.");
					}
				}
			});
		});
	});
}
