
HTML2PDF

Generation of PDF files for nodes using html2pdf http://www.tufat.com/s_html2ps_html2pdf.htm


To install, place the entire module folder into your modules directory.

Download html2pdf and place it in directory .../modules/html2pdf/html2pdf/
Replace files 

with files from
.../modules/html2pdf/html2pdf_update 

Go to Administer -> Site Building -> Modules and enable the HTML2PDF module.

To change module settings go to
Administer -> Site Configuration -> HTML2PDF

To set up access to pdf files go to
Administer -> User Management -> Access Control
